Source: powershell-empire
Section: misc
Priority: optional
Maintainer: Parrot Dev Team <team@parrotsec.org>
Uploaders: Parrot Dev Team <team@parrotsec.org>
Build-Depends: debhelper-compat (= 12), python3, python3-pip, gcc, libpython3-dev, python3-poetry
Standards-Version: 4.5.1
Homepage: https://github.com/BC-SECURITY/Empire

Package: powershell-empire
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-poetry,
         mariadb-server
#Suggests: openjdk for jar payloads
#Suggests: Nimzippy, NimCrypto
Suggests: nim, libnim-winim-dev, mingw-w64
Recommends: powershell
Description: PowerShell and Python post-exploitation agent
 This package contains a post-exploitation framework that includes a
 pure-PowerShell2.0 Windows agent, and a pure Python Linux/OS X agent.
 It is the merge of the previous PowerShell Empire and Python EmPyre projects.
 The framework offers cryptologically-secure communications and a flexible
 architecture. On the PowerShell side, Empire implements the ability to run
 PowerShell agents without needing powershell.exe, rapidly deployable
 post-exploitation modules ranging from key loggers to Mimikatz, and adaptable
 communications to evade network detection, all wrapped up in a
 usability-focused framework.
